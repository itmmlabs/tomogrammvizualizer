﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TomogrammVizualizer
{
    class View
    {
        private int min = 0;
        private int max = 3000;

        private Bitmap textureImage;
        private int VBOtexture = 1;

        public void setMin(int m)
        {
            min = m;
        }

        public void setMax(int m)
        {
            max = m;
        }

        public void LoadTexture()
        {
            GL.BindTexture(TextureTarget.Texture2D, VBOtexture);
            BitmapData data = textureImage.LockBits(
                new System.Drawing.Rectangle(0, 0, textureImage.Width, textureImage.Height),
                ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb
            );
            
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, 
                data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
                PixelType.UnsignedByte, data.Scan0);

            textureImage.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, 
                (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);

            ErrorCode Er = GL.GetError();
            string str = Er.ToString();
        }

        public void generateTextureImage(int layerNumber)
        {
            textureImage = new Bitmap(Bin.X, Bin.Y);
            for (int i = 0; i < Bin.X; ++i)
            {
                for (int j = 0; j < Bin.Y; j++)
                {
                    int pixelNumber = i + j * Bin.X + layerNumber * Bin.X * Bin.Y;
                    textureImage.SetPixel(i, j, TransferFunction(Bin.array[pixelNumber]));
                }
            }
        }

        public void DrawTexture()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, VBOtexture);

            GL.Begin(BeginMode.Quads);
            GL.Color3(Color.White);
            GL.TexCoord3(0f, 0f, 0);
            GL.Vertex3(0, 0, 0);
            GL.TexCoord2(0f, 1f);
            GL.Vertex3(0, 1f, 0);
            GL.TexCoord3(1f, 1f, 0);
            GL.Vertex3(1f, 1f, 0);
            GL.TexCoord3(1f, 0f, 0);
            GL.Vertex3(1f, 0, 0);

            GL.End();

            GL.Disable(EnableCap.Texture2D);

        }

        public void SetupView(int width, int height)
        {   
            GL.ShadeModel(ShadingModel.Smooth);
    

            Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(
                MathHelper.PiOver3,
                1,
                1f,
                3f);

            GL.MatrixMode(MatrixMode.Projection);

            GL.LoadMatrix(ref perspective);

            Vector3 cameraPosition = new Vector3(0.5f, 0.5f, 1);
            Vector3 cameraDirection = new Vector3(0.5f, 0.5f, -0.5f);
            Vector3 cameraUp = new Vector3(0, 1f, 0f);

            Matrix4 lookAt = Matrix4.LookAt(cameraPosition, cameraDirection, cameraUp);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref lookAt);

            //GL.LoadIdentity();
            //GL.Ortho(0, 1, 1, 0, -1, 1);

            int newWidth, newHeight, offsetX, offsetY;

            if (width > height)
            {
                newHeight = height;
                newWidth = height;
                offsetX = (width - height) / 2;
                offsetY = 0;
            }
            else
            {
                newHeight = width;
                newWidth = width;
                offsetY = (height - width) / 2;
                offsetX = 0;
            }

            GL.Viewport(offsetX, offsetY, newWidth, newHeight);
            //GL.Viewport(0, 0, width, height);

       
        }

        Color TransferFunction(short value)
        {
            if (min >= max)
            {
                return Color.Black;
            }
            int newVal = clamp((value - min) * 255 / (max - min), 0, 255);

            return Color.FromArgb(newVal, newVal, newVal);
        }


        public void DrawQuadStrips(int layerNumber)
        {
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);

            int x_coord = 0, y_coord = 0;
            short value; // = Bin.array[x_coord + y_coord * Bin.X + layerNumber * Bin.X * Bin.Y];

            for (y_coord = 0; y_coord < Bin.Y - 1; y_coord++)
            {
                GL.Begin(BeginMode.QuadStrip);
                for (x_coord = 0; x_coord < Bin.X; x_coord++)
                {
                    value = Bin.array[x_coord + y_coord * Bin.X + layerNumber * Bin.X * Bin.Y];

                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)x_coord / Bin.X, (float)y_coord / Bin.Y, 0);

                    value = Bin.array[x_coord + (y_coord + 1) * Bin.X + layerNumber * Bin.X * Bin.Y];

                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)x_coord / Bin.X, (float)(y_coord + 1) / Bin.Y, 0);

                }
                GL.End();
            }

            //GL.Color3(TransferFunction(value));
        }

        public void DrawQuads(int layerNumber)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            /*Matrix4 modelview = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelview);*/

            GL.PushMatrix();
            GL.Rotate(45, Vector3.UnitY);

            GL.Begin(BeginMode.Quads);

            for (int x_coord = 0; x_coord < Bin.X - 1; x_coord++)
            {
                for (int y_coord = 0; y_coord < Bin.Y - 1; y_coord++)
                {
                    short value;
            
                    value = Bin.array[x_coord + y_coord * Bin.X + layerNumber * Bin.X * Bin.Y];
            
                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)x_coord / Bin.X, (float)y_coord / Bin.Y, 0);
            
                    value = Bin.array[x_coord + (y_coord + 1) * Bin.X + layerNumber * Bin.X * Bin.Y];
            
                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)x_coord / Bin.X, (float)(y_coord+1) / Bin.Y, 0);
            
                    value = Bin.array[x_coord + 1 + (y_coord + 1) * Bin.X + layerNumber * Bin.X * Bin.Y];
            
                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)(x_coord + 1) / Bin.X, (float)(y_coord + 1) / Bin.Y, 0);
            
                    value = Bin.array[x_coord + 1 + (y_coord) * Bin.X + layerNumber * Bin.X * Bin.Y];
            
                    GL.Color3(TransferFunction(value));
                    GL.Vertex3((float)(x_coord + 1) / Bin.X, (float)y_coord / Bin.Y, 0);
            
                }
            }
           
            GL.End();

            
            GL.PopMatrix();
        }

        int clamp(int v, int min, int max)
        {
            if (v > max)
            {
                return max;
            } else if (v < min)
            {
                return min;
            }

            return v;
        }
    }
}