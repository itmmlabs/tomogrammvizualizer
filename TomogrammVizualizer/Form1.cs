﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TomogrammVizualizer
{
    public partial class Form1 : Form
    {
        Bin bin = new Bin();
        private View view = new View();
        private bool loaded = false;
        private int currentLayer = 0;

        private bool needReload = true;

        private int FrameCount;
        private DateTime nextFPSUpdate;

        private int mode = 0;

        private int curWidth;
        private int curHeight;

        public Form1()
        {
            InitializeComponent();
        }

        private void displayFPS()
        {
            if (DateTime.Now >= nextFPSUpdate)
            {
                this.Text = String.Format("CT Vizualizer (fps={0}) ", FrameCount);

                nextFPSUpdate = DateTime.Now.AddSeconds(1);
                FrameCount = 0;
            }

            FrameCount++;
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string str = dialog.FileName;
                bin.readBIN(str);
                trackBar1.Maximum = Bin.Z - 1;
                trackBar1.Minimum = 0;
                view.SetupView(glControl1.Width, glControl1.Height);
                loaded = true;
                glControl1.Invalidate();
            }
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (loaded)
            {
//                view.DrawQuads(currentLayer);
//                glControl1.SwapBuffers();
                switch (mode)
                {
                        case 0:
                            view.DrawQuads(currentLayer);
                            break;
                        case 1:
                            if (needReload)
                            {
                                view.generateTextureImage(currentLayer);
                                view.LoadTexture();
                                needReload = false;
                            }
                            view.DrawTexture();
                            break;
                        case 2:
                            view.DrawQuadStrips(currentLayer);
                            break;
                }
                
                glControl1.SwapBuffers();
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            currentLayer = trackBar1.Value;
            needReload = true;
        }

        void App_Idle(object sender, EventArgs e)
        {
            while (glControl1.IsIdle)
            {
                displayFPS();
                glControl1.Invalidate();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Application.Idle += App_Idle;
            curWidth = this.Width;
            curHeight = this.Height;
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            view.setMin(this.trackBar2.Value);
            needReload = true;
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            view.setMax(trackBar3.Value);
            needReload = true;
        }

        private void quadsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mode = 0;
            quadsToolStripMenuItem.CheckState = CheckState.Checked;
            quadStripToolStripMenuItem.CheckState = CheckState.Unchecked;
            texturesToolStripMenuItem.CheckState = CheckState.Unchecked;
        }

        private void texturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mode = 1;
            quadsToolStripMenuItem.CheckState = CheckState.Unchecked;
            quadStripToolStripMenuItem.CheckState = CheckState.Unchecked;
            texturesToolStripMenuItem.CheckState = CheckState.Checked;
        }

        private void quadStripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mode = 2;
            quadsToolStripMenuItem.CheckState = CheckState.Unchecked;
            quadStripToolStripMenuItem.CheckState = CheckState.Checked;
            texturesToolStripMenuItem.CheckState = CheckState.Unchecked;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (curWidth != 0)
            {

                int dx = Width - curWidth;
                int dy = Height - curHeight;

                this.glControl1.Width += dx;
                this.glControl1.Height += dy;

                this.trackBar1.Width += dx;
                this.trackBar2.Width += dx;
                this.trackBar3.Width += dx;

                curWidth = Width;
                curHeight = Height;

                view.SetupView(glControl1.Width, glControl1.Height);
            }
        }
    }
}
